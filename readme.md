# DAM Appcelerator Build Automator

### Version 3.0

Build automation tool for SK app. Can be modified for other applicaitons.

## Process

Run `bash appc-build.sh` to do the following:

* Remove any old files from the `oddOutputDirectory` and `evenOutputDirectory` if they exist or create them
* Clone the repository into a sub folder if needed
* Switch to proper branch
* Update the `tiapp.xml` file in the root directory with the next version number (Odd: UAT, Even: Test)
* Update `./app/lib/data-client.js` based on the current version number by commenting and uncommenting the url lines based on the version number in `tiapp.xml`
* Commit changes to the previous files
* Build the project using Titanuim/Appcelerator CLI
* Apply version tagging
* Push to repository with tags
* Migrate builds to locally synchronized shared directory

## IF A BUILD FAILS OUT

* Validate if the build number was rolled back on failure.
* If it was not rolled back, run `bash rollback.sh`
* All builds generated from the failed run will be deleted

## Config guide

`local.cfg` - These values need to be changed for each individual machine
`config.cfg` - These are project configuration settings and should only be changed from project to project

Both config files must reside in the root directory where `appc-build.sh` is run.

** local.cfg **

* `gitUser` - User name for accessing git repository in the format `USER@ADDRESS.ORG`
* `loglevel` - Log level for build output. Supported options are `trace`, `debug`, `info`, `warn`, and `error`
* `repoDir` - Local directory for repository created using `git clone [REPO] repoDir`. Should not be blank. This will be created if it does not exist.
* `outRoot` - Output directory root file. Odd and Even directories are placed in this path. Must contain a trailing `/`
* `oddOutputDirectory` - Directory for completed odd version builds
* `evenOutputDirectory` - Directory for completed even version builds
* `sharePath` - Path to synchronized share folder on local machine. This is for distribution purposes.
* `androidSdkPath` - Path to Android SDK
* `androidKeystoreLocation` - Path to keystore

** config.cfg **

The following items control Git functions:

* `protocol` - Either `http` or `https`. Used for the git repo
* `gitRepo` - Web URL for git repo
* `gitBranch` - Branch to target for builds
* `tagPrefix` - Prefix to use for each tag, such as `beta` or `v`
* `tagDelim` - Delimiter for each word in tag
* `tagMessage` - Message to attach to tag append
* `commitMessage` - Message to attach to commit

The following items should be unique to each project:

* `targetFile` - File to find in repo that denotes the root directory. This should be unique for the project.
* `evenURL` - URL to activate for even number builds. Only needs the first part of the url `http://sub.sub.main.com/` up to the part that would change (see the included config)
* `oddURL` - URL to activate for odd number builds. See above for instructions

The following items are unique for Appcelerator projects:

* `username` - Appcelerator username
* `password` - Appcelerator password
* `target` - Target OS to build for. Accepts `android`, `ios`, or `both`.
* `totalBuilds` - Total number of builds to run per version
* `expectedFileName` - Name of the complete build file without the extension. This is used for validating build creation.

** Always review the config file settings first if there is an issue **

** THE $ SYMBOL MUST BE ESCAPED FROM ANY VALUES IN THE CONFIG FILE **

### iOS Required Fields for Appcelerator

The following are only required for building iOS apps:

* `iosDistName` - Distribution name recognized by Appcelerator
* `iosPPUID` - UUID for certificate

### Android Required Fields for Appcelerator

The following are only required when building Android apps:

* `androidKeystorePassword` - Keystore password
* `androidKeystoreAlias` - Alias for the keystore