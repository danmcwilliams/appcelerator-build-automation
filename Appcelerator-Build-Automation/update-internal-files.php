<?php
/**
 * Created by PhpStorm.
 * User: ryanbrown
 * Date: 11/11/15
 * Time: 1:34 PM
 * 
 * Run this script with one argument at the front for the root of the project
 */
include_once "AppcBuilder.php";

$build = new AppcBuilder();
if($build->loadConfig('local.cfg', 'config.cfg', $argv[1]) == -1){
    exit(1);
}

echo("[*] UPDATE VERSION" . PHP_EOL);
$tiappFileResult = $build->updateTiappFile();
if ($tiappFileResult < 0) {
    exit(0);
}

// Update data-client.js
$dataClientFileResult = -1;
$dataClientFileResult = $build->updateDataClientFile($tiappFileResult);

if($dataClientFileResult < 0){
    exit(0);
}

exit($dataClientFileResult);

