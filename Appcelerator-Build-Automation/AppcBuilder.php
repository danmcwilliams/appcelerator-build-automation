<?php

class AppcBuilder
{

    private $_ROOT, $tiappFile, $dataClientFile, $config;

    function getRoot(){return $this->_ROOT;}
    function getTiappFile(){ return $this->tiappFile;}
    function getDataClientFile(){return $this->dataClientFile;}
    function getConfig(){return $this->config;}

    // loadConfig
    // Loads the config into the object
    // $projectConfig - Config settings for project - do not alter (config.cfg)
    // $localConfig - Config settings for local computer (local.cfg)
    // $projectRoot - The root of the actual project
    function loadConfig($localConfig, $projectConfig, $projectRoot){
        try {
            $this->config = parse_ini_file($localConfig);
            $tmp = parse_ini_file($projectConfig);
            $this->config = array_merge($this->config, $tmp);
        } catch (ErrorException $e) {
            echo("[E] Invalid or no config files found in this directory." . PHP_EOL);
            return -1;
        }
        if(!$this->validateConfig($this->config)){
            echo("[E] Invalid config files found in this directory." . PHP_EOL);
            return -1;
        }

        $this->_ROOT = $projectRoot; // File project root
        $this->tiappFile = $this->_ROOT . "/" . $this->config["targetFile"];
        $this->dataClientFile = $this->_ROOT . "/app/lib/data-client.js";
        return 0;
    }
    // END loadConfig

    // validateConfig
    // $config - config file JSON OBJECT (NOT FILE PATH - use loadConfig for that)
    // Returns true if config file is valid, false if it is not
    function validateConfig($config){
        try{
        	if($config["protocol"] == "http" || $config["protocol"] == "https"){
        	}
        	else{
        		echo "[E] - Error in protocol";
        		return false;
        	}
        	if($config["gitRepo"] == ""){
        		echo "[E] - Error in gitRepo";
        		return false;
        	}
        	if($config["gitUser"] == ""){
        		echo "[E] - Error in gitUser";
        		return false;
        	}
        	if($config["gitBranch"] == ""){
        		echo "[E] - Error in gitBranch";
        		return false;
        	}
        	if($config["repoDir"] == ""){
        		echo "[E] - Error in repoDir";
        		return false;
        	}
        	if($config["targetFile"] == ""){
        		echo "[E] - Error in targetFile";
        		return false;
        	}
            if($config["evenURL"] == ""){
            	echo "[E] - Error in evenURL";
                return false;
            }
            if($config["oddURL"] == ""){
            	echo "[E] - Error in oddURL";
                return false;
            }
            if($config["loglevel"] == ""){
            	echo "[E] - Error in loglevel";
                return false;
            }
            if($config["username"] == ""){
            	echo "[E] - Error in username";
                return false;
            }
            if($config["password"] == ""){
            	echo "[E] - Error in password";
                return false;
            }
            if($config["totalBuilds"] < 1){
            	echo "[E] - Error in totalBuilds";
        		return false;
        	}
        	if($config["expectedFileName"] == ""){
        		echo "[E] - Error in expectedFileName";
        		return false;
        	}
            if($config["oddOutputDirectory"] == ""){
            	echo "[E] - Error in oddOutputDirectory";
                return false;
            }
            if($config["evenOutputDirectory"] == ""){
            	echo "[E] - Error in evenOutputDirectory";
                return false;
            }
            if($config["target"] == "ios" || $config["target"] == "both"){
                if($config["iosDistName"] == ""){
                	echo "[E] - Error in iosDistName";
                    return false;
                }
                if($config["iosPPUID"] == ""){
                	echo "[E] - Error in iosPPUID";
                    return false;
                }
            }
            if($config["target"] == "android" || $config["target"] == "both"){
                if($config["androidSdkPath"] == ""){
                	echo "[E] - Error in androidSdkPath";
                    return false;
                }
                if($config["androidKeystoreLocation"] == ""){
                	echo "[E] - Error in androidKeystoreLocation";
                    return false;
                }
                if($config["androidKeystorePassword"] == ""){
                	echo "[E] - Error in androidKeystorePassword";
                    return false;
                }
                if($config["androidKeystoreAlias"] == ""){
					echo "[E] - Error in androidKeystoreAlias";
					return false;
                }
            }
            if($config["target"] == "ios" || $config["target"] == "android" || $config["target"] == "both"){
                return true;
            }
            echo "[E] - Unknown error in config.";
            return false;
        }
        catch(ErrorException $e){
            return false;
        }
    }
    // END validateConfig

    // updateDataClientFile
    // $clientFile - Path to data-client.js
    // $osSwitch - Resultant subnet number from updateTiappFile. Determines
    // the server to activate for this build.
    // $iosUrl, $androidUrl - URLs for Android and iOS environments
    // This will modify the data-client.js file and return 0 or -1 for
    // success or failure.
    function updateDataClientFile($buildNumber)
    {
        $config = $this->config;
        $clientFile = $this->dataClientFile;
        echo("[*] MODIFY HTTP PATHS" . PHP_EOL);
        if (file_exists($clientFile)) {

            switch ($buildNumber % 2) {
                case 0: // Evens
                    $activeSite = $config["evenURL"];
                    $inactiveSite = $config["oddURL"];
                    break;
                default: // Odds
                    $activeSite = $config["oddURL"];
                    $inactiveSite = $config["evenURL"];
                    break;
            }
            echo("[*] Active target url: " . $activeSite . PHP_EOL);

            // File opening
            $tmpFile = $clientFile . ".tmp";
            try {
                $ifstream = fopen($clientFile, "r");
                $ofstream = fopen($tmpFile, "w+");
            } catch (ErrorException $e) {
                echo("[E] Cannot open " . $clientFile . " " . $e->getMessage() . PHP_EOL);
                return -1;
            }

            // RW loop
            $lncount = 0;
            $commentChanges = false;
            $slashReg = "/^(\s*\/\/)(?!:(.|\s)*)/";
			// $slashReg = "#//(?!\w+)#";
            while (($line = fgets($ifstream)) !== false) {
                $lncount++;
                if (strpos($line, $activeSite) && preg_match($slashReg , $line)) { // Uncomment these lines
                    echo("[*] Line " . $lncount . ": This will be uncommented:" . $line);
                    $line = preg_replace($slashReg , "", $line, 1);
                    $commentChanges = true;
                }
                elseif (strpos($line, $inactiveSite) && !preg_match($slashReg , $line)) { // Comment out these lines
                    echo("[*] Line " . $lncount . ": This will be commented:" . $line);
                    $line = "//" . $line;
                    $commentChanges = true;
                }

                fwrite($ofstream, $line);
            }

            // Close files
            fclose($ofstream);
            fclose($ifstream);

            // Replace file if changed
            if (!$commentChanges) {
                echo("[*] Current URL is active" . PHP_EOL);
                echo("[*] No code was changed in " . $clientFile . PHP_EOL);
                try {
                    shell_exec("rm " . $tmpFile);
                } catch (ErrorException $e) {
                    echo("[E] File cleanup unsuccessful - " . $e);
                    return -1;
                }
                return $buildNumber;
            } else {
                try {
                    shell_exec("cp " . $tmpFile . " " . $clientFile);
                    shell_exec("rm " . $tmpFile);
                    echo("[*] " . $clientFile . " has been updated." . PHP_EOL);
                    echo("[*] Current URL is active" . PHP_EOL);
                    return $buildNumber;
                } catch (ErrorException $e) {
                    echo("[E] File cleanup unsuccessful - " . $e);
                    return -1;
                }
            }
        } else {
            echo('[E] ' . $clientFile . ' does not exist.' . PHP_EOL);
            return -1;
        }
    }
    // END updateDataClientFile


    // updateTiappFileCustom
    // $tiappFile - Path to target file
    // $increment - Amount to increment version subnet by
    // This will modify the file upon completion and return the new
    // final subnet or -1 on error.
    // RECOMMEND PUBLICLY USING updateTiappFile INSTEAD OF THIS
    function updateTiappFileCustom($tiappFile, $increment)
    {
        if (file_exists($tiappFile)) {

            try {
                $xml = simplexml_load_file($tiappFile);
            } catch (ErrorException $e) {
                echo("[E] Cannot read file at " . $tiappFile . PHP_EOL);
                return -1;
            }
            $currentVersion = $xml->version;
            echo("[*] Current Version: " . $currentVersion . PHP_EOL);

            // Get final subnet value
            preg_match("/\d+$/", $xml->version, $versionSubnet);

            // Update version subnet by increment
            $newVersionSubnet = intval($versionSubnet[0]) + $increment;
            $newVersion = preg_replace("/\d+$/", strval($newVersionSubnet), $currentVersion);
            echo("[*] New Version:     " . $newVersion . PHP_EOL);

            // Update XML file
            $xml->version = $newVersion;
            try {
                $xml->saveXML($tiappFile);
                echo("[*] " . $tiappFile . " has been changed to Version " . $newVersion . PHP_EOL);
                return $newVersionSubnet;
            } catch (ErrorException $e) {
                echo ("[E] Cannot save file: " . $e->getMessage()) . PHP_EOL;
                return -1;
            }

        } else {
            echo('[E] ' . $tiappFile . ' does not exist.' . PHP_EOL);
            return -1;
        }
    }
    // END updateTiappFile

    // helper function to automatically update by 1
    function updateTiappFile()
    {   $tiappFile = $this->tiappFile;
        return $this->updateTiappFileCustom($tiappFile, 1);
    }

}

?>