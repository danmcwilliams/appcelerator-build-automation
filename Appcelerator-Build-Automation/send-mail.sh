#!/bin/bash

# @params $1 - Project name
# @params $2 - Status
# @params $3 - Version

# EMAIL GLOBALS
SCRIPT_START=$(date +%D\ %r);
SCRIPT_START_S=$(date -u +"%s");
MAIL_SUBJECT="$1 - BUILD STATUS UPDATE - $2 - Version $3";
SMS_MAIL_SUBJECT="$1 BUILD UPDATE"
MAIL_FROM="devops@develappme.com"
BRK="\n-------------------------------------------------------------------------------------\n";
REPLACE_LN="\n";

# CARRIER DOMAINS
TMOBILE="tmomail.net"
ATT="txt.att.net"
SPRINT="messaging.sprintpcs.com"
# END CARRIER DOMAINS

# RECIPIENTS
# Just using lazy index-matched arrays for this
# RECIPIENTS_MAIL=("dmcwilliams@develappme.com" "rbrown@develappme.com")
# RECIPIENTS_PHONE=("4044315017" "2244021817")
# RECIPIENTS_CARRIER=("$ATT" "$TMOBILE")
RECIPIENTS_MAIL=("rbrown@develappme.com dmcwilliams@develappme.com gorlin@develappme.com")
RECIPIENTS_PHONE=("2244021817 4044315017 7703171088")
RECIPIENTS_CARRIER=("$TMOBILE $ATT $SPRINT")
# END RECIPIENTS

# htmlHeading()
# @params $1 - The heading number 1-5
# @params $2 - The string content to place inside the header
# @returns - string
function htmlHeading(){
	echo "<h$1>$2</h$1>";
}

# htmlParagraph()
# @params $1 - The string content to place inside the paragraph
# @returns - string
function htmlParagraph(){
	echo "<p>${1}</p>";
}
# htmlDocOpen()
# @returns - string
function htmlDocOpen(){
	echo "<!DOCTYPE html><html><head><title>${MAIL_SUBJECT}</title></head><body>";
}
# htmlDocClose()
# @returns -string
function htmlDocClose(){
	echo "</body></html>";
}

hrDate(){
	env TZ='America/New_York'
}
# END GLOBALS


# mailExecutioner
# @params $1 - The string html content
# VERIFIED WORKING
mailExecutioner(){
	TITLE="$1"
	MSG="$2"
	RECIPIENT="$3"
	printf "${BRK}${BRK}$TITLE\n$MSG\n\n";
	INNER_HEAD=$(htmlHeading 2 "${TITLE}");
	INNER_MSG=$(htmlParagraph "${MSG}");
	BODY="${INNER_HEAD}${INNER_MSG}";
	HEAD=${htmlDocOpen};
	FOOT=${htmlDocClose};
	HTML="${HEAD}${BODY}${FOOT}";
	# Linux
 	# mail -s "$(echo -e "$MAIL_SUBJECT\nContent-Type: text/html")" "$RECIPIENT" -a "From: $MAIL_FROM" <<< "$HTML";
	# iOS
	cp /dev/null message.txt
	echo "Subject: $MAIL_SUBJECT" >> message.txt
	echo "$TITLE" >> message.txt
	echo "" >> message.txt
	echo "$MSG" >> message.txt
	sendmail -f "$MAIL_FROM" "$RECIPIENT" < message.txt
	rm message.txt
}

# smsExecutioner
# VERIFIED WORKING WITH ATT
smsExecutioner(){
	TITLE="$1"
	MSG="$2"
	PHONE="$3"
	CARRIER="$4"
	RECIPIENT="$PHONE@$CARRIER"
	HTML="$TITLE $MSG";
 	# Linux
 	# mail -s "$SMS_MAIL_SUBJECT" "$RECIPIENT" -a "From: $MAIL_FROM" <<< "$HTML";
 	# iOS
 	cp /dev/null message.txt
	echo "Subject: $SMS_MAIL_SUBJECT" >> message.txt
	echo "$TITLE $MSG" >> message.txt
	sendmail -f "$MAIL_FROM" "$RECIPIENT" < message.txt
	rm message.txt
}

testAllExecutors(){
	echo "Testing email..."
	mailExecutioner "Test Email from $(hostname)" "Hello this is a test message!" "rybo27@gmail.com"
	echo "Testing sms..."
	smsExecutioner "Test SMS from $(hostname)" "This works from my iMac tho" "4044315017" "$ATT"
}
