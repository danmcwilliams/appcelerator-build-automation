<?php
/**
 * Created by PhpStorm.
 * User: ryanbrown
 * Date: 11/11/15
 * Time: 2:20 PM
 */

include_once "AppcBuilder.php";

$build = new AppcBuilder();
if($build->loadConfig('config.cfg') == -1){
    exit(1);
}

echo("[*] ROLLING BACK VERSION" . PHP_EOL);
$build->updateTiappFileCustom($build->getTiappFile(), -1);