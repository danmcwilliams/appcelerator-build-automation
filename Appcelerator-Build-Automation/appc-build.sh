#!/usr/bin/env bash
source config.cfg
source local.cfg

SCRIPT_START=$(date +%D\ %r);
echo "[*] SCRIPT START: $SCRIPT_START"
# Validate config file
php validate-config.php
eCode=$?

if [ $eCode == 1 ]; then
	echo "[E] CONFIG FILE INVALID"
	SCRIPT_END=$(date +%D\ %r);
	echo "[*] SCRIPT END: $SCRIPT_END"
	exit 1
else
	echo "[*] CONFIG FILE VALIDATED"
fi

evenOutputDirectory="$outRoot$evenOutPutDirectory"
oddOutputDirectory="$outRoot$oddOutPutDirectory"

# Git work here
# Repo setup
fullGitRepo="$protocol://$gitUser/$gitRepo"
echo "[*] TARGETING GIT AT: $fullGitRepo"

didClone=false
if [ -d "$repoDir" ]; then
	echo "[*] Repo directory exists"
else
	git clone "$fullGitRepo" "$repoDir"
	didClone=true
fi

# Switch to branch
workingDir=( $(pwd) )
echo "[*] MOVING TO $repoDir FOR GIT UPDATES"
cd "$repoDir"
pullResult=$(git fetch && git checkout "$gitBranch")
echo "[*] CURRENT PROJECT BRANCH:"
git branch

# Exit if there have been no code changes
if [[ "$pullResult" == *"Your branch is up-to-date"* ]] && [ "$didClone" = false ]; then
	echo "[!] No code changes, no build."
	SCRIPT_END=$(date +%D\ %r);
	echo "[*] SCRIPT END: $SCRIPT_END"
	exit 0
fi

git pull origin "$gitBranch"
pullResult=$(git fetch && git checkout "$gitBranch")
echo "[*] CURRENT PROJECT BRANCH:"
git branch
echo "[*] MOVING BACK TO $workingDir FOR BUILD"
cd "$workingDir"

# Find project root and set
targetFilePath=( $(find . -name "$targetFile" -print0) )

if [ -z "$targetFilePath" ]; then
	echo "[E] $targetFile not found"
	SCRIPT_END=$(date +%D\ %r);
	echo "[*] SCRIPT END: $SCRIPT_END"
	exit 1
else
	echo "[*] $targetFile located at: $targetFilePath"
	root=( $(dirname $targetFilePath) )
	echo "[*] Project root set to: $root"
fi
# End Git work

###################
# MAIN BUILD LOOP #
###################
COUNTER=0
while [[ COUNTER -lt "$totalBuilds" ]]; do

	# Make sure directories are created
	# Clean out anything old in the directories
	if [ -d "$evenOutputDirectory" ]; then
		echo "[*] CLEANING OUT $evenOutputDirectory"
		if [ -n "$(ls -A $evenOutputDirectory)" ]; then
			rm -r "$evenOutputDirectory"/*
		fi
	else
		echo "[*] CREATING $evenOutputDirectory"
		mkdir -p "$evenOutputDirectory"
	fi

	if [ -d "$oddOutputDirectory" ]; then
		echo "[*] CLEANING OUT $oddOutputDirectory"
		if [ -n "$(ls -A $oddOutputDirectory)" ]; then
			rm -r "$oddOutputDirectory"/*
		fi
	else
		echo "[*] CREATING $oddOutputDirectory"
		mkdir -p "$oddOutputDirectory"
	fi
	
    # Update version number and data-client.js
    php update-internal-files.php "$root"
    version=$?
    if [ $version != 0 ]; then
	
	  # Commit to git
      cd "$repoDir"
      echo "[*] Committing version change to git"
      git commit -a -m "$commitMessage"
      cd "$workingDir"
      
      oddoreven=$(($version % 2))
      if [ $oddoreven == 0 ]; then
        outDir="$evenOutputDirectory"
      else
        outDir="$oddOutputDirectory"
      fi

      uniqueBuilds=1
      # Setup to do both builds
      buildBoth="false"
      if [ "$target" == "both" ]; then
        target="ios"
        uniqueBuilds=2
        buildBoth="true"
        echo "[*] Builds for iOS and Android will be generated..."
      fi
      
      #####################
      # UNIQUE BUILD LOOP #
      #####################
      BUILDCOUNTER=0
      runOnceAfterLogin="true"
      while [[ BUILDCOUNTER -lt "$uniqueBuilds" ]]; do
          echo "[*] APPCELERATOR LOGIN"
          echo "[*] Logging out for safety..."
          appc logout
          Expect login.exp "$username" "$password"
          echo "[*] Login completed"

          if [ "$runOnceAfterLogin" == "true" ]; then
            appc ti clean --project-dir "$root"
            runOnceAfterLogin="false"
          fi

          if [ "$target" == "ios" ]; then
            appc ti build --platform "$target" --f --project-dir "$root" --output-dir "$outDir" --log-level "$loglevel" --target dist-adhoc --distribution-name "$iosDistName" --pp-uuid "$iosPPUID"
            fileExt=".ipa"
          fi
          if [ "$target" == "android" ]; then
            appc run --platform "$target" --f --project-dir "$root" --output-dir "$outDir" --log-level "$loglevel" --target dist-playstore --android-sdk "$androidSdkPath" --keystore "$androidKeystoreLocation" --store-password "$androidKeystorePassword" --alias "$androidKeystoreAlias"
            fileExt=".apk"
          fi

          if [ -a "$outDir/$expectedFileName$fileExt" ]; then
            echo "[*] Build created at $outDir"
            echo "[*] BUILD SUCCEEDED"
            # Create Git Tag
            cd "$repoDir"
            echo "[*] Creating git tag $tagPrefix$tagDelim$target$tagDelim""Build""$tagDelim$version"
            git tag -a "$tagPrefix$tagDelim$target$tagDelim""Build""$tagDelim$version" -m "$tagMessage"
            cd "$workingDir"
          else
            echo "[E] No build found at $outDir/$expectedFileName$fileExt"
            bash rollback.sh
            # Remove all builds
            rm -r "$evenOutputDirectory"/*
            rm -r "$oddOutputDirectory"/*
            echo "[E] BUILD FAILED"
            SCRIPT_END=$(date +%D\ %r);
	  		echo "[*] SCRIPT END: $SCRIPT_END"
            exit 1
          fi

          # Switch OS if doing both builds
          if [ "$buildBoth" == "true" ]; then
            target="android"
          fi
          let BUILDCOUNTER=BUILDCOUNTER+1
      done
      #########################
      # END UNIQUE BUILD LOOP #
      #########################

    # Reset build to both if buildBoth
    if [ "$buildBoth" == "true" ]; then
        target="both"
    fi

    else
      echo "[E] ABORTED: FAILURE TO UPDATE INTERNAL FILES"
      SCRIPT_END=$(date +%D\ %r);
	  echo "[*] SCRIPT END: $SCRIPT_END"
      exit 1
    fi

	if [ -d "$sharePath$version" ]; then
		echo "[W] - File and directory exists at $sharePath$version. Deleting."
		rm -rf "$sharePath$version"
	fi
	
    echo "[*] Copying files to shared drive at $sharePath$version"
    mkdir "$sharePath$version"
    mv "$outRoot" "$sharePath$version"
    
    # Commit to git and push
    cd "$repoDir"
    # Safety commit
    git commit -a -m "$commitMessage"
    echo "[*] Pushing git commits"
	git push origin "$gitBranch" --follow-tags
    cd "$workingDir"
    
    let COUNTER=COUNTER+1
done
#######################
# END MAIN BUILD LOOP #
#######################

SCRIPT_END=$(date +%D\ %r);
echo "[*] SCRIPT END: $SCRIPT_END"